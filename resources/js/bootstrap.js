window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');

console.log(process.env.MIX_PUSHER_APP_KEY, process.env.MIX_PUSHER_APP_CLUSTER);

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    forceTLS: true,
    auth: {
        headers: {
            Authorization: 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxMSIsImp0aSI6Ijg3ZjQ0OGNlNjI5ZTkzOTllMjFmNGM4YTNiNjYxNmY3OTIxYmMyMDU5YzE1ZDE3MGI2YWFjMzU2OTRmNjhjZWUwZmFlZGQ3MjBiOWQ2MjI5IiwiaWF0IjoxNjM1MzEzOTcwLjc0OTc3MiwibmJmIjoxNjM1MzEzOTcwLjc0OTc3NiwiZXhwIjoxNjM2MTc3OTcwLjYzMDM5NSwic3ViIjoiMSIsInNjb3BlcyI6W119.J5Ne0UaTXTeFydDuruy1V7pSENWwaj3mpelOfHToIqbNzwz8plMTY2ejDbFWgM-pjqcT4UtqAYSlwbqpr3a3u9Qi7J12jEHlmO6o70i-Ke1VBjiDblWXFkApk-oJAO_zgsddjedTqv7FVrNVSUPlMKFmTlCzOfOCmjFvEmf3D24oz-L7U6pz_JSrBTjS14XxavUqhWry88I_rSlhO8w2KwBpEpu-RVKWzB0FOFdPq1Gi6eBAgN33RgnbxAoloyPRA_XsqeHL8laDPZNc_ECB8s4GmMbUYGdYctho_-o49lExLJN8n9dg93fUGNRa09jmbyuqAMhqXnNw7NocK6kNJaO8g8Uxpvk8Ed9AEV8I23cgHpp7tAIJBsLr1Qk580SPWSLUNreDhmYfTBFAlP0cknovdAUDwmLAkPWZBf1VcRiarHslz5q-S0MR0WjUthqXVkAtCc93vsI6ILlygOsN0I5z9bk1aDg8XeTHRdrDL3eSu31JVWxMwjGAhDriw0hyQZsw2Uwkp4ktM5TJ5FD1mByhFv757bq1N2UWDMPlyepqO9Fbi-9irXD5xhUA_TvbA5Q39pGMnZqgGYVsBgh5no6o7385sW3FAS5kbfX1WvUKkCYvamWRPJxPTA2oBfc3-2ZBaBTLwpCf34Ix8v5lEZRXnxK7TiwAc6Cx8XTJGOo'
        },
    },
});