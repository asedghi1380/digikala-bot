@servers(['web_development' => ['sso@52.65.240.174'], 'web_production' => ['deployer@130.185.120.226']])

@setup

    $username = isset($uname) ? $uname : "deployer";
    $branch = isset($branch) ? $branch : "develop";

    $repository = 'git@gitlab.com:zamanLtd/server/sso.git';
    $releases_dir = '/home/'.$username.'/id.engenesis.com/releases';
    $docker_releases_dir='/var/www/html/releases';
    $docker_app_dir = '/var/www/html';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
    $new_docker_release_dir = $docker_releases_dir .'/'. $release;


    function logMessage($message) {
        return "echo '\033[32m" .$message. "\033[0m';\n";
    }

@endsetup

@story('deploy_dev', ['on' => ['web_development']])
    clone_repository_dev
    run_composer
{{--    migrate_database--}}
{{--    install_passport--}}
    run_npm_backend
    run_npm_frontend
    restart_queue
    clear_cache
    link_current
    update_symlinks
    clean_old_releases
@endstory



@story('deploy_prod', ['on' => ['web_production']])
    clone_repository_prod
    run_composer
    update_symlinks
    migrate_database

    restart_queue
    clear_cache
    clean_old_releases
@endstory


@story('rollback')
    rollback
@endstory


@task('build_images')
    echo 'Building container images'
    cd {{ $new_release_dir }}
    export APP_MOUNT={{ $release_mount }}
    docker-compose up --build -d
@endtask


@task('clone_repository_dev')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} --branch={{ $branch }} {{ $new_release_dir }}
@endtask

@task('clone_repository_prod')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    docker exec -i sso-app sh
    pwd
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_docker_release_dir }}
{{-- --no-dev --}}
    composer install --no-ansi --no-interaction --no-plugins --no-progress --no-scripts --optimize-autoloader
@endtask

@task('update_symlinks')
    docker exec -i sso-app sh
    echo "Linking storage directory"
    rm -rf {{ $new_docker_release_dir }}/storage
    ln -nfs {{ $docker_app_dir }}/storage {{ $new_docker_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $docker_app_dir }}/.env {{ $new_docker_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_docker_release_dir }} {{ $docker_app_dir }}/current
@endtask

@task('migrate_database')
    docker exec -i sso-app sh
    echo "Migrating database"
    cd {{ $docker_app_dir }}/current
    php artisan down
    php artisan migrate:ordered --fresh --seed
    php artisan up
@endtask

@task('restart_queue')
    docker exec -i sso-app sh
    echo "Restart Queue"
    cd {{ $docker_app_dir }}/current
    php artisan queue:restart
@endtask

@task('clear_cache')
    docker exec -i sso-app sh
    echo "Clear Cache"
    cd {{ $docker_app_dir }}/current
    php artisan optimize:clear
    php artisan cache:clear
@endtask

@task('rollback')
    docker exec -i sso-app sh
    cd {{ $new_docker_release_dir }}
    ln -nfs {{ $new_docker_release_dir }}/$(find . -maxdepth 1 -name "20*" | sort  | tail -n 2 | head -n1) {{ $docker_app_dir }}/current
    echo "Rolled back to $(find . -maxdepth 1 -name "20*" | sort  | tail -n 2 | head -n1)"
@endtask

@task('clean_old_releases')
    docker exec -i sso-app sh
    # Delete all but the 3 most recent releases
    {{ logMessage("Cleaning old releases") }}
    cd {{ $docker_releases_dir }}
    ls -dt {{ $docker_releases_dir }}/* | tail -n +4 | xargs  rm -rf;
@endtask




@task('run_npm_backend')
    docker exec -i sso-app sh
    if [ "$(diff -qr  {{ $new_docker_release_dir }}/Modules/PanelCore/Resources/assets/ {{ $docker_app_dir }}/current/Modules/PanelCore/Resources/assets/)" != "" ] ||  [  "$(diff -q {{ $new_docker_release_dir }}/Modules/PanelCore/package.json {{ $docker_app_dir }}/current/Modules/PanelCore/package.json)" != "" ] ||  [  "$(diff -q {{ $new_docker_release_dir }}/Modules/PanelCore/tailwind.config.js {{ $docker_app_dir }}/current/Modules/PanelCore/tailwind.config.js)" != "" ]; then
        echo "Install panel NPM dependencies"
        cd {{ $new_docker_release_dir }}/Modules/PanelCore/
        npm i
        npm run prod
    else
        echo "no modified copying old files"
        cp -r {{ $docker_app_dir }}/current/public/dist  {{ $new_docker_release_dir }}/public/dist
        cp {{ $docker_app_dir }}/current/public/mix-manifest.json  {{ $new_docker_release_dir }}/public/mix-manifest.json
        chown 1001:1001  {{ $new_docker_release_dir }}/public/mix-manifest.json
    fi
@endtask

@task('run_npm_frontend')
    docker exec -i sso-app sh
    if [ "$(diff -qr  {{ $new_docker_release_dir }}/Modules/Front/Resources/assets/ {{ $docker_app_dir }}/current/Modules/Front/Resources/assets/)" != "" ] ||  [  "$(diff -q {{ $new_docker_release_dir }}/Modules/Front/package.json {{ $docker_app_dir }}/current/Modules/Front/package.json)" != "" ]; then
        echo "Install front NPM dependencies"
        cd {{ $new_docker_release_dir }}/Modules/Front/
        npm i
        npm run prod
    else
        echo "no modified copying old files"
        cp -r {{ $docker_app_dir }}/current/public/css  {{ $new_docker_release_dir }}/public/css
        cp -r {{ $docker_app_dir }}/current/public/js  {{ $new_docker_release_dir }}/public/js
        cp -r {{ $docker_app_dir }}/current/public/images  {{ $new_docker_release_dir }}/public/images
        cp {{ $docker_app_dir }}/current/public/mix-manifest.json   {{ $new_docker_release_dir }}/public/mix-manifest.json
        chown 1001:1001  {{ $new_docker_release_dir }}/public/mix-manifest.json
    fi
@endtask

@task('link_current')
    docker exec -i sso-app sh
    echo 'Linking current release'
    ln -nfs {{ $new_docker_release_dir }} {{ $docker_app_dir }}/current
    echo 'linking public storage'
    cd {{ $new_docker_release_dir }}
    php artisan storage:link
@endtask



@task('install_passport')
    docker exec -i sso-app sh
    # Install passport
    cd {{ $docker_app_dir }}/current
    {{ logMessage("Installing passport") }}
    php artisan passport:install --force
@endtask