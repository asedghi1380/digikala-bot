<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title_fa');
            $table->string('title_en');
//            $table->json('url');
            $table->string('status');
            $table->boolean('has_quick_view');
//            $table->integer('images_count');
            $table->json('rating');
//            $table->json('properties');
//            $table->json('videos');
            $table->foreignId('category_id')->nullable()->constrained('categories');
            $table->foreignId('brand_id')->nullable()->constrained('brands');

//            $table->json('review');

//            $table->json('pros_and_cons');
            $table->json('suggestion');

//            $table->integer('concurrent_viewers');
            $table->integer('questions_count');
            $table->integer('comments_count');

//            $table->json('size_guide')->nullable();
            $table->json('breadcrumb');
//            $table->json('specifications');
//            $table->json('expert_reviews');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
