<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained('products');
            $table->string('title')->nullable();
            $table->text('body');
            $table->double('rate');
            $table->json('reactions');
            $table->json('files');
            $table->enum('recommendation_status',['not_recommended', 'no_idea', 'recommended'])->nullable();
            $table->boolean('is_buyer');
            $table->string('user_name');
            $table->boolean('is_anonymous');
            $table->json('purchased_item');
            $table->json('advantages');
            $table->json('disadvantages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
};
