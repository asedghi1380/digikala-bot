<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->id();
            $table->double('lead_time');
            $table->double('rank');
            $table->double('rate');
            $table->json('statistics')->nullable();
            $table->string('status');
//            $table->json('properties');
//            $table->json('digiplus');
            $table->foreignId('color_id')->nullable()->constrained('colors');
            $table->foreignId('warranty_id')->constrained('warranties');
            $table->foreignId('seller_id')->constrained('sellers');
            $table->foreignId('product_id')->constrained('products');

//            $table->json('digiclub');
            $table->json('price');
//            $table->json('shipment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
};
