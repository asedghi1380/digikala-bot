<?php

use Illuminate\Support\Facades\Route;
use \JsonMachine\Items;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/import', [\App\Http\Controllers\MigrateController::class,'migrate']);
//Route::get('/com', [\App\Http\Controllers\CommentMigrateController::class,'migrate']);
//
//
//
//Route::get('/cats', [\App\Http\Controllers\DataController::class,'cats']);
//
//Route::get('/sellers', [\App\Http\Controllers\DataController::class,'sellers']);


