<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'engenesis' => [
        "app_id" => env('ENGENESES_APP_ID'),
        "app_secret" => env('ENGENESIS_APP_SECRET'),
        "app_url" => env('ENGENESIS_APP_URL')
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_CALLBACK_URL', 'http://example.com/callback-url'),
    ],

    'linkedin' => [
        'client_id' => env('LINKEDIN_CLIENT_ID'),
        'client_secret' => env('LINKEDIN_CLIENT_SECRET'),
        'redirect' => env('LINKEDIN_CLIENT_CALLBACK_URL', 'http://localhost:8000/auth/linkedin/callback'),
    ],

    'passport' => [
        'login_endpoint' => env('PASSPORT_TOKEN_LOGIN_ENDPOINT'),
        'grant_type' => env('PASSPORT_TOKEN_LOGIN_GRANT_TYPE'),
        'client_id' => env('PASSPORT_TOKEN_CLIENT_ID'),
        'client_secret' => env('PASSPORT_TOKEN_CLIENT_SECRET'),
    ],
    'infusionSoft' => [
        'INFUSIONSOFT_CLIENT_ID' => env('INFUSIONSOFT_CLIENT_ID'),
        'INFUSIONSOFT_SECRET' => env('INFUSIONSOFT_SECRET'),
        'INFUSIONSOFT_REDIRECT_URL' => env('INFUSIONSOFT_REDIRECT_URL'),
    ]


];
