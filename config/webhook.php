<?php

return [
   'applications' => [
       'being-profile' => [
           'url' => env('BEING_PROFILE_APP_URL'),
           'app_secret' => env('BEING_PROFILE_APP_SECRET'),
           'active' => env('BEING_PROFILE_APP_Active')
       ],
       'platform' => [
           'url' => env('PLATFORM_APP_URL'),
           'app_secret' => env('PLATFORM_APP_SECRET'),
           'active' => env('PLATFORM_APP_Active')
       ],
   ]
];