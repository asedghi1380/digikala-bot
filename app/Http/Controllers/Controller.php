<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


/**
 * @OA\Info(
 *   version="0.1",
 *   title="Flitt Api Documention",
 *   description="Documentation for Flitt app",
 *   @OA\License(
 *     name="Apache 2.0",
 *     url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *   )
 * )
 *
 * @OA\Server(
 *   url=L5_SWAGGER_CONST_HOST_API_V_1,
 *   description="Serveur"
 * )
 *
 * @OA\SecurityScheme(
 *   type="apiKey",
 *   securityScheme="bearer",
 *   name="Authorization",
 *   scheme="bearer",
 *   description="Enter token in format (Bearer <token>)",
 *   in="header"
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
