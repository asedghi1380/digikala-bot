<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Comment;
use App\Models\Product;
use App\Models\Seller;
use App\Models\Size;
use App\Models\Variant;
use App\Models\Warranty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use JsonMachine\Items;

class CommentMigrateController extends Controller
{

    public function migrate()
    {

        $j = 0;
        $folder=0;
        for ($product_id = 11274522 ; $product_id <= 22549058; $product_id++) {

            $productsCommentPages = Items::fromFile(public_path("data/comments/product${product_id}.json"));
            try {
                foreach ($productsCommentPages as $commentPage) {
                    foreach ($commentPage as $comment)
                        $this->create($comment, $product_id);
                }
            } catch (\Exception $exception) {
                dd($product_id,$exception);
            }

        }
        dd("finish");

    }


    public function create($comment, $product_id)
    {
//        $product = $this->createProduct($product->data->product, $brand?->id, $category?->id);
        $product = Product::query()->find($product_id);
        if ($product)
            $this->createComment($comment, $product_id);
    }

    public function createComment($data, $product_id)
    {
        return Comment::firstOrCreate([
            'id' => $data->id
        ], array_merge((array)$data, [
            'product_id' => $product_id,
        ]));
    }
}
