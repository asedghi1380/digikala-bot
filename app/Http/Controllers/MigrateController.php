<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Comment;
use App\Models\Product;
use App\Models\Seller;
use App\Models\Size;
use App\Models\Variant;
use App\Models\Warranty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use JsonMachine\Items;

class MigrateController extends Controller
{

    public function migrate()
    {

        $j = 0;
        $folder=0;
        for ($i = 3411 ; $i <= 4828; $i++) {

            $products = Items::fromFile(public_path("data-server/data${i}.json"));
            Log::debug("importing file $i and remaining: " . 4828-$i);

            try {
                foreach ($products as $product) {
                    $this->create($product);
                }
            } catch (\Exception $exception) {
                Log::debug($i.' - '.$exception->getMessage());
            }

        }
        Log::debug('finished');

    }


    public function create($product)
    {
        $category = $this->createCategory($product->data->product->category);
        $brand = $this->createBrand($product->data->product->brand);
        $p = $this->createProduct($product->data->product, $brand?->id, $category?->id);
        $this->createVariants($product->data->product->variants, $p->id);
//        $comment = $this->createComment($product->data->)
    }

    public function createProduct($data, $brand_id, $category_id)
    {


        return Product::firstOrCreate([
            'id' => $data->id
        ], array_merge((array)$data, [
            'brand_id' => $brand_id,
            'category_id' => $category_id,
        ]));

    }

    public function createVariants($data, $product_id)
    {


        foreach ($data as $variant) {
            if (isset($variant->color)) {
                $color_id = $this->createColor($variant?->color)->id;
            }
            $warranty = $this->createWarranty($variant->warranty);
            $seller = $this->createSeller($variant->seller);
            $ee = array_merge((array)$variant,
                [
                    'product_id' => $product_id,
                    'color_id' => $color_id ?? null,
                    'seller_id' => $seller->id,
                    'warranty_id' => $warranty->id
                ]
            );
            Variant::firstOrCreate([
                'id' => $variant->id
            ], $ee);
        }
    }

    public function createWarranty($data)
    {
        return Warranty::firstOrCreate([
            'id' => $data->id
        ], (array)$data);
    }

    public function createCategory($data)
    {
        if (isset($data->id)) {
            return Category::firstOrCreate([
                'id' => $data->id
            ], (array)$data);
        }

        return null;
    }


    public function createColor($data)
    {
        return Color::firstOrCreate([
            'id' => $data->id
        ], (array)$data);
    }

    public function createSize($data)
    {
        return Size::firstOrCreate([
            'id' => $data->id
        ], (array)$data);
    }

    public function createBrand($data)
    {
        if (isset($data->id)) {

            return Brand::firstOrCreate([
                'id' => $data->id
            ], (array)$data);
        }

        return null;
    }

    public function createSeller($data)
    {
        return Seller::firstOrCreate([
            'id' => $data->id
        ], (array)$data);
    }

}
