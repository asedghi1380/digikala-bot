<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Seller;
use App\Models\Variant;
use Illuminate\Http\Request;

class DataController extends Controller
{

    public function cats()
    {

        ini_set('memory_limit', -1);

        $data[] = ['cat_id', 'cat_title', 'product_count', 'comments_count', 'paid_count', 'sellers', 'brands'];

        Category::select('id', 'title_fa')
            ->whereIn('categories.id', function ($query) {
                $query->from('products')
                    ->select('products.category_id')
                    ->where('products.status', 'marketable');
            })
            ->with('products:id,status,category_id,brand_id,comments_count', 'products.variants.seller', 'products.brand')->chunk(7, function ($categories) use (&$data) {


                foreach ($categories as $category) {

                    $sellers = $this->getSallers($category);
                    $brands = $this->getBrands($category);
                    $data[] = [
                        $category->id,
                        $category->title_fa,
                        $category->products->count(),
                        $category->products->sum('comments_count'),
                        $category->products->sum('paid_count'),
                        json_encode($sellers, JSON_UNESCAPED_UNICODE),
                        json_encode($brands, JSON_UNESCAPED_UNICODE),

                    ];
                }
            });


        $this->createCSV('cat_products', $data);
    }


    public function sellers()
    {
        $data[] = ['seller_id', 'seller_name', 'product_count', 'comments_count', 'paid_count', 'categories', 'brands'];

        Seller::with('variants')->chunk(7, function ($sellers) use (&$data) {

            foreach ($sellers as $seller) {
                var_dump($seller->id);
                var_dump('<br/>');
                $products = Product::with('brand','category')->whereIn('id', $seller->variants->groupBy('product_id')->keys())->get();
                $brands = $this->getBrandsFromProducts($products);
                $categories = $this->getCategories($products);
                $data[] = [
                    $seller->id,
                    $seller->title,
                    $products->count(),
                    $products->sum('comments_count'),
                    $products->sum('paid_count'),
                    json_encode($categories, JSON_UNESCAPED_UNICODE),
                    json_encode($brands, JSON_UNESCAPED_UNICODE),

                ];
            }
        });
        $this->createCSV('sellers_products', $data);


    }

    public function createCSV($filename, $data)
    {


        $handle = fopen(public_path("exports/$filename.csv"), 'w');

        foreach ($data as $row) {
            fputcsv($handle, $row, ',');
        }
        fclose($handle);
    }

    /**
     * @param mixed $category
     * @return mixed
     */
    public function getSallers(mixed $category): mixed
    {
        $sellers = $category->products->map(function ($product) {
            return $product->variants->map(function ($variant) {

                return [
                    'seller_id' => $variant->seller->id, 'name' => $variant->seller->title, 'stars' => $variant->seller->stars, 'rating' => $variant->seller->rating
                ];
            });
        })->flatten(1)->groupBy('seller_id');

        return array_values($sellers->map(function ($seller, $d) {
            return $seller[0] + ['product_cat_count' => count($seller)];
        })->toArray());

    }

    /**
     * @param mixed $entity
     * @return mixed
     */
    public function getBrands(mixed $entity): mixed
    {
        $brands = $entity->products->whereNotNull('brand_id')->map(function ($product) {
            return [
                'brand_id' => $product->brand->id, 'brand_name' => $product->brand->title_fa
            ];

        })->groupBy('brand_id');


        return array_values($brands->map(function ($brand) {

            return $brand[0] + ['product_cat_count' => count($brand)];
        })->toArray());

    }

    /**
     * @param mixed $products
     * @return mixed
     */
    public function getBrandsFromProducts(mixed $products): mixed
    {
        $brands = $products->whereNotNull('brand_id')->map(function ($product) {
            return [
                'brand_id' => $product->brand->id, 'brand_name' => $product->brand->title_fa
            ];

        })->groupBy('brand_id');


        return array_values($brands->map(function ($brand) {

            return $brand[0] + ['product_cat_count' => count($brand)];
        })->toArray());

    }

    /**
     * @param mixed $entity
     * @return mixed
     */
    public function getCategories(mixed $products): mixed
    {
        $brands = $products->whereNotNull('category_id')->map(function ($product) {
            return [
                'category_id' => $product->category->id, 'category_name' => $product->category->title_fa
            ];

        })->groupBy('category_id');


        return array_values($brands->map(function ($brand) {

            return $brand[0] + ['cat_count' => count($brand)];
        })->toArray());

    }

}


