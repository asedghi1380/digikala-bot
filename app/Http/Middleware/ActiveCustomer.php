<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ActiveCustomer
{
    use \App\Traits\ResponseTrait;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->is_active == 0) {
            return $this->response('Your account is not active');

        }
        return $next($request);
    }
}
