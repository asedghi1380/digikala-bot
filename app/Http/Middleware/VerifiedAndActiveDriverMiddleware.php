<?php

namespace App\Http\Middleware;

use App\Traits\ResponseTrait;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class VerifiedAndActiveDriverMiddleware
{
    use ResponseTrait;

    public function handle(Request $request, Closure $next)
    {
        if ($this->checkUser()) {
            return $this->response('This action is unauthorized', code: Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }

    private function checkUser()
    {
        return auth('api-driver')->check() &&
            (!(isset(auth('api-driver')->user()->mobile_verified_at) && auth('api-driver')->user()->is_active));
//        return auth('api-driver')->check() &&
//            (!(isset(auth('api-driver')->user()->email_verified_at)
//                && isset(auth('api-driver')->user()->mobile_verified_at)
//                && auth('api-driver')->user()->is_active));
    }
}