<?php

namespace App\Http\Middleware;

use App\Traits\ResponseTrait;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class VerifiedAndActiveUserMiddleware
{
    use ResponseTrait;

    public function handle(Request $request, Closure $next)
    {
        if (auth('api')->check() &&
            (!(isset(auth('api')->user()->email_verified_at)
                && isset(auth('api')->user()->mobile_verified_at)
                && auth('api')->user()->is_active))) {
            return $this->response('This action is unauthorized', code: Response::HTTP_FORBIDDEN);
        }


        return $next($request);
    }
}