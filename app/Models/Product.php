<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public $timestamps=false;


    protected $fillable=[
        'title_fa',
        'title_en',
        'slug',
        'status',
        'has_quick_view',
//        'images',
        'rating',
        'default_variant_id',
//        'properties',
//        'videos',
//        'url',
        'category_id',
        'brand_id',
//        'review',
//        'pros_and_cons',
        'suggestion',
        'questions_count',
        'comments_count',
        'breadcrumb',
//        'specifications',
//        'concurrent_viewers',
//        'expert_reviews',
        'id',
    ];

    protected $casts = [
//        'images' => 'array',
//        'url' => 'array',
        'rating' => 'array',
//        'properties' => 'array',
//        'videos' => 'array',
//        'review' => 'array',
//        'pros_and_cons' => 'array',
        'suggestion' => 'array',
        'breadcrumb' => 'array',
//        'specifications' => 'array',
//        'expert_reviews' => 'array',
    ];

    public function defaultVariant(){
        return $this->hasOne(Variant::class);
    }
    public function variants(){
        return $this->hasMany(Variant::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function colors(){
        return $this->belongsToMany(Color::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function questions(){
        return $this->hasMany(Question::class);
    }

    public function getPaidCountAttribute(){
        return $this->comments_count *7000;
    }
}
