<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    use HasFactory;
    protected $fillable=[
        'title',
        'code',
        'url',
        'rating',
        'properties',
        'stars',
        'id',
    ];

    protected $casts = [
        'properties' => 'array',
        'rating' => 'array',
    ];

    public function variants(){
        return $this->hasMany(Variant::class);
    }

}
