<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $casts = [
        'url' => 'array',
        'logo' => 'array',
    ];
    protected $fillable=[
        'id',
        'code',
        'title_fa',
        'title_en',
        'url',
        'visibility',
        'url',
        'logo',
        'is_premium',
        'is_miscellaneous',
        'is_name_similar',
    ];

    public function products(){
        return $this->hasMany(Product::class);
    }
}
