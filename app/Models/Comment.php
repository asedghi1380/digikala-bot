<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'product_id',
        'title',
        'body',
        'rate',
        'reactions',
        'files',
        'recommendation_status',
        'is_buyer',
        'user_name',
        'is_anonymous',
        'purchased_item',
        'advantages',
        'disadvantages',
    ];
    protected $casts = [
        'reactions' => 'array',
        'files' => 'array',
        'purchased_item' => 'array',
        'advantages' => 'array',
        'disadvantages' => 'array',
    ];

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
