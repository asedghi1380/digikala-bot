<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    use HasFactory;
    public $timestamps=false;

    protected $fillable=[
        'id',
        'is_default',
        'product_id',
        'lead_time',
        'rank',
        'rate',
        'statistics',
        'status',
//        'properties',
//        'digiplus',
        'color_id',
        'warranty_id',
        'seller_id',
//        'digiclub',
        'price',
//        'shipment_methods',
    ];

    protected $casts = [
        'statistics' => 'array',
//        'properties' => 'array',
//        'digiplus' => 'array',
        'price' => 'array',
//        'digiclub' => 'array',
//        'shipment_methods' => 'array',
    ];

    public function seller(){
        return $this->belongsTo(Seller::class);
    }

    public function warranty(){
        return $this->belongsTo(Warranty::class);
    }

    public  function color(){
        $this->belongsTo(Color::class);
    }

    public  function product(){
        $this->belongsTo(Product::class);
    }

}
