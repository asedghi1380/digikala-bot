<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable=[
        'id',
        'title_fa',
        'title_en',
        'code',
        'content_description',
    ];
    public function products(){
        return $this->hasMany(Product::class);
    }
}
