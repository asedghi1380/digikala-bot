<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    use HasFactory;

    protected $fillable=[
        'id',
        'title_en',
        'title_fa'
    ];
    public function variants(){
        return $this->hasMany(Variant::class);
    }
}
