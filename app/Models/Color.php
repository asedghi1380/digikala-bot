<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    use HasFactory;


    protected $fillable=[
        'id',
        'title',
        'hex_code'
    ];

    public function Products(){
        return $this->hasMany(Product::class);
    }
}
