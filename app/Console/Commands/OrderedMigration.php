<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Entities\Permission;
use Modules\Admin\Entities\Role;

class OrderedMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:ordered  {--seed} {--fresh} {--oauth}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ordered migration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $important_modules = [
            'PanelCore',
            'RegionLanguage',
            'Admin',
        ];

        $modules = [
            'PanelCore',
            'RegionLanguage',
            'User',
            'Payment',
            'Applications',
            'Admin',
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

//         || $this->confirm('Do you want reset migration?', false)
        if ($this->option('fresh'))
        {
            Artisan::call("migrate:reset");
            $this->info("resting migrations...");
        }

        $bar = $this->output->createProgressBar(count($modules));
        $this->info("start creating migrations...");
        $bar->start();
        foreach ($modules as $module)
        {
            Artisan::call("module:migrate $module --force");
            $bar->advance();
        }
        $bar->finish();

//|| $this->confirm('Do you want to run seed ?', false)
        if ($this->option('seed'))
        {
            $this->line("");
            $this->info("start creating seeding...");
            $bar = $this->output->createProgressBar(count($modules));
            $bar->start();
            foreach ($modules as $module)
            {
                Artisan::call("module:seed $module");
                $bar->advance();
            }
            $bar->finish();
            $this->line("");
        } else {
            $this->line("");
            $this->info("seed Just Admin Module For Start...");
            $bar = $this->output->createProgressBar(count($important_modules));
            $bar->start();
            foreach ($important_modules as $module) {
                Artisan::call("module:seed $module");
                $bar->advance();
            }
            $bar->finish();
            $this->line("");
        }


        Artisan::call("migrate");

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
