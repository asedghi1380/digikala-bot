<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Validation\ValidationException;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {

        if ($request->expectsJson()) {
            return $this->response(['message' => $exception->getMessage()], 401);
        }
//        redirect()->guest($exception->redirectTo() ?? route('login'))
        if ($request->is('panel') || $request->is('panel/*')) {
            return redirect()->guest('/panel/login');
        }

        return redirect()->guest(route('login'));
    }

    public function render($request, Throwable $e)
    {
        if ($e instanceof ValidationException) {
            if ($request->expectsJson()) {
                return $this->response($e->validator->errors()->messages(), Response::HTTP_BAD_REQUEST);
            }
        }
        if ($e instanceof HttpException) {
            if ($request->expectsJson()) {
                return $this->response($e->getMessage(), Response::HTTP_FORBIDDEN);
            }
        }
        if ($e instanceof AuthorizationException) {
            return $this->response($e->getMessage(), 403);
        }
        if ($e instanceof BadRequestException) {
            return $this->response('Bad Request', 400);
        }
        if ($e instanceof OAuthServerException) {
            if ($request->expectsJson()) {
                return $this->response($e->getMessage(), 400);
            }
        }

        if ($e instanceof ThrottleRequestsException) {
            if ($request->expectsJson()) {
                return $this->response($e->getMessage(), 429);
            }
        }

        return parent::render($request, $e);
    }

    public function report(Throwable $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }
}
